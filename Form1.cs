﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace String_editors
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ToEdit = textBox1.Text;
            string Edited = Reverse(ToEdit);
            textBox2.Text = Edited;
            button4.Visible = false;
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string ToEdit = textBox1.Text;
            string Edited = Palindrome(ToEdit);
            if (Edited.Equals(ToEdit))
            {
                textBox2.Text = ToEdit + " is a palindrome." + System.Environment.NewLine + "Because " + ToEdit + " Backwards is " + Edited;
            }
            else
            {
                textBox2.Text = ToEdit + " is not a palindrome." + System.Environment.NewLine + "Because " + ToEdit + " Backwards is " + Edited;
            }
            button4.Visible = false;
        }

        public static string Palindrome(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string ToEdit = textBox1.Text;
            string[] toPig = ToEdit.Split(' ');
            StringBuilder translated = new StringBuilder();
            foreach (string word in toPig)
            {
                string temp = Pig(word);
                translated.Append(" " + temp);
            }
            string Fin = translated.ToString(0, translated.Length);
            textBox2.Text = Fin;
            button4.Visible = true;
        }

        public static string Pig(string str)
        {
            string firstletter = str.Substring(0, 1);
            string RestOfWord = str.Substring(1, str.Length - 1);
            string vowels = "AEIOUaeiou";
            int letterPos = vowels.IndexOf(firstletter);
            if (letterPos == -1)
            {
                string Secondletter = str.Substring(1, 1);
                int letterPos2 = vowels.IndexOf(Secondletter);
                StringBuilder toEnd = new StringBuilder(firstletter);
                if (letterPos2 == -1)
                {
                    toEnd.Append(Secondletter);
                    string ThirdLetter = str.Substring(2, 1);
                    int letterPos3 = vowels.IndexOf(ThirdLetter);
                    RestOfWord = RestOfWord.Substring(1, RestOfWord.Length - 1);

                    if (letterPos3 == -1)
                    {
                        RestOfWord = RestOfWord.Substring(1, RestOfWord.Length - 1);
                        toEnd.Append(ThirdLetter);
                    }
                }
                str = RestOfWord + "-" + toEnd + "ay";
            }
            else
            {
                str = str + "-way";
            }
            return (str);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string ToEdit = textBox2.Text;
            
            string[] toPig = ToEdit.Split(' ');
            StringBuilder translated = new StringBuilder();
            MessageBox.Show(toPig[0] + "  " + toPig[1]);
            foreach (string word in toPig)
            {
                MessageBox.Show("Word Equals" + word);
                string temp = PigDecode(word);
                translated.Append(" " + temp);
            }
            string Fin = translated.ToString(0, translated.Length);
            textBox1.Text = Fin;

        }

        public static string PigDecode(string str)
        {
            string[] toDo = str.Split('-');
            MessageBox.Show(toDo[0]);
            StringBuilder removal = new StringBuilder(toDo[1]);
            //removal.Remove(0, 1);
            removal.Remove(removal.Length - 2, 2);
            string w = "w";
            string ending = removal.ToString(0, removal.Length);
            string firstLetter = ending.Substring(0, 1);
            int letterPos = w.IndexOf(firstLetter);
            if (letterPos == -1)
            {
                toDo[0] = ending + toDo[0];
            }
            else
            {
                string firstLetter2 = toDo[0].Substring(0, 1);
                string vowels = "AEIOUaeiou";
                if (firstLetter2.IndexOf(vowels) == -1)
                {
                    toDo[0] = "w" + toDo[0];
                }

            }
            return (toDo[0]);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string originalText = textBox1.Text;
            StringBuilder sb = new StringBuilder(originalText);

            sb.Replace(".", "._._._");
            sb.Replace("a", "._");
            sb.Replace("b", "_...");
            sb.Replace("c", "_._.");
            sb.Replace("d", "_..");
            sb.Replace("e", ".");
            sb.Replace("f", ".._.");
            sb.Replace("g", "__.");
            sb.Replace("h", "....");
            sb.Replace("i", "..");
            sb.Replace("j", ".___");
            sb.Replace("k", "_._");
            sb.Replace("l", "._..");
            sb.Replace("m", "__");
            sb.Replace("n", "_.");
            sb.Replace("o", "___");
            sb.Replace("p", ".__.");
            sb.Replace("q", "__._");
            sb.Replace("r", "._.");
            sb.Replace("s", "...");
            sb.Replace("t", "_");
            sb.Replace("u", ".._");
            sb.Replace("v", "..._");
            sb.Replace("w", ".__");
            sb.Replace("x", "_.._");
            sb.Replace("y", "_.__");
            sb.Replace("z", "__..");

            sb.Replace("A", "._");
            sb.Replace("B", "_...");
            sb.Replace("C", "_._.");
            sb.Replace("D", "_..");
            sb.Replace("E", ".");
            sb.Replace("F", ".._.");
            sb.Replace("G", "__.");
            sb.Replace("H", "....");
            sb.Replace("I", "..");
            sb.Replace("J", ".___");
            sb.Replace("K", "_._");
            sb.Replace("L", "._..");
            sb.Replace("M", "__");
            sb.Replace("N", "_.");
            sb.Replace("O", "___");
            sb.Replace("P", ".__.");
            sb.Replace("Q", "__._");
            sb.Replace("R", "._.");
            sb.Replace("S", "...");
            sb.Replace("T", "_");
            sb.Replace("U", ".._");
            sb.Replace("V", "..._");
            sb.Replace("W", ".__");
            sb.Replace("X", "_.._");
            sb.Replace("Y", "_.__");
            sb.Replace("Z", "__..");

            sb.Replace("0", "_____");
            sb.Replace("1", ".____");
            sb.Replace("2", "..___");
            sb.Replace("3", "...__");
            sb.Replace("4", "...._");
            sb.Replace("5", ".....");
            sb.Replace("6", "_....");
            sb.Replace("7", "__...");
            sb.Replace("8", "___..");
            sb.Replace("9", "____.");
            sb.Replace(",", "__..__");
            sb.Replace("?", "..__..");
            sb.Replace("'", ".____.");
            sb.Replace("!", "_._.__");
            sb.Replace("/", "_.._.");
            sb.Replace("(", "_.__.");
            sb.Replace(")", "_.__._");
            sb.Replace("&", "._...");
            sb.Replace(":", "___...");
            sb.Replace(";", "_._._.");
            sb.Replace("=", "_..._");
            sb.Replace("+", "._._.");
            sb.Replace("-", "_...._");
            sb.Replace("\"", "._.._.");
            sb.Replace("$", "..._.._");
            sb.Replace("@", ".__._.");

            string final = sb.ToString(0, sb.Length);
            textBox2.Text = final;
        }


    }
}
